try {
   var _window = window.top;
   var _document = window.top.document;
} catch (err) {
   console.log(err); 
   _window = window.parent;
   _document = window.parent.document;
}
var EGMobileInPage = function (config) {
   _root = this;
   this.config = config;
   this.data = {
       site_content: null,
       obj_div_content: null,
       client_width: null,
       main_div: null,
       div_mid: null,
       ad_content: null,
       ad_click: null,
       check_create: null,
       site_config: {
           id_div_content: null,
           rule: null,
           show_before: 0,
           is_detect: true
       },
       rotate: "portrait",
       portrait: {
           is_play: false,
       },
       landscape: {
           is_play: false,
       },
   };
   EGMobileInPage.prototype.init = function () {
       _root.data.site_content = _document;
       var hostname = _window.location.host;
       if (hostname.indexOf('www.') === 0) {
           hostname = hostname.replace('www.', '');
       }
       if (hostname == undefined) {
           return;
       }
       if (typeof mobilefullscreen_site_config[hostname].div_content != "undefined") {
           _root.data.site_config.id_div_content = mobilefullscreen_site_config[hostname].div_content;
       } else {
       }
       //id rule
       if (typeof mobilefullscreen_site_config[hostname].rule != "undefined") {
           _root.data.site_config.rule = mobilefullscreen_site_config[hostname].rule;
       } else {
       }
       //is_detect
       if (typeof mobilefullscreen_site_config[hostname].is_detect != "undefined") {
           _root.data.site_config.is_detect = mobilefullscreen_site_config[hostname].is_detect;
       }
       if (_root.data.site_config.is_detect == true) {
           _root.analyticContent();
       } else {
           _root.createBanner(null);
       }
   };
   EGMobileInPage.prototype.analyticContent = function () {
       try {
           _root.data.site_content.getElementById(_root.data.site_config.id_div_content) ? _root.data.site_content.getElementById(_root.data.site_config.id_div_content).id : _root.data.site_content.getElementsByClassName(_root.data.site_config.id_div_content)[0].setAttribute('id', _root.data.site_config.id_div_content);
           _root.data.obj_div_content = _document.getElementById(_root.data.site_config.id_div_content);
           //check video content
           var arr_rule = ['p', 'div', 'span'];
           var rule_length = 0;
           for (var i = 0; i < arr_rule.length; i++) {
               var element = arr_rule[i];
               var rule = _root.data.obj_div_content.querySelectorAll(element);
               // console.log(rule);
               if (rule.length > rule_length) {
                   _root.data.site_config.rule = element;
                   _root.data.site_config.show_before = Math.floor(rule.length / 3);
                   rule_length = rule.length;
               }
           }
           _root.getPosition();
       } catch (error) {
           console.log(error); 
       }
   };
   EGMobileInPage.prototype.getPosition = function () {
       //run banner
       if (!_root.data.obj_div_content) return false;
       var parent = null;
       var flag = _root.data.obj_div_content.getElementsByTagName(_root.data.site_config.rule).item(_root.data.site_config.show_before);
       if (flag !== null) {
           parent = flag.parentNode;
           flag = _root.contentSplit(parent, flag);
           _root.data.client_width = _root.wdWidth();
           _root.createBanner(flag);
       } else {
           if (_root.data.site_config.show_before > 1) {
               _root.data.site_config.show_before--;
               _root.getPosition();
           } else {
           }
       }
   };
   EGMobileInPage.prototype.createBanner = function (flag) {
       var mainDiv = document.createElement('div');
       mainDiv.setAttribute('id', 'EG-EGMobileInPage-wrap')
       mainDiv.style.width = '100%';
       mainDiv.style.height = '100%';
       mainDiv.style.maxWidth = '100%';
       mainDiv.style.overflow = 'initial';
       mainDiv.style.textAlign = 'center';
       mainDiv.style.position = 'relative';
       mainDiv.style.background = 'transparent';
       mainDiv.style.visibility = 'visible';
       mainDiv.style.display = 'block';
       mainDiv.style.opacity = '1';
       mainDiv.style.zIndex = '2';
       var divMid = document.createElement('div');
       divMid.style.width = '100%';
       divMid.style.height = '100%';
       divMid.style.overflow = 'hidden';
       divMid.style.textAlign = 'center';
       divMid.style.position = 'absolute';
       divMid.style.background = 'transparent';
       //divMid.style.clip = 'rect(0 360px 640px 0px)';
       _root.data.div_mid = divMid;
       mainDiv.appendChild(divMid);
       var ad_content = document.createElement('div');
       ad_content.setAttribute('id', 'ns-mobile-inpage');
       ad_content.style.opacity = 1;
       ad_content.style.transform = ' perspective(0)';
       if (_root.data.obj_div_content != null) {
           ad_content.style.width = _root.data.obj_div_content.clientWidth + "px";
       }
       ad_content.style.height = "100%";
       _root.data.ad_content = ad_content;
       var ad_click = document.createElement('div');
       ad_click.style.cursor = "pointer";
       ad_click.setAttribute('id', 'mobile-inpage-click');
       ad_click.style.width = "100%";
       ad_click.style.height = "100%";
       var arrRefer = _document.location;
       
       var ogrin = arrRefer.protocol + '//' + arrRefer.hostname;
       ad_click.innerHTML = '<iframe scrolling="no" style="border:0px;width:100% !important;height:100% !important d" id="eg-mobile-portrait" width="100%" height="100%" src="' + _root.config.url_banner + '?origin=' + ogrin + '&clickTAG=' + encodeURIComponent(_root.config.click_redirect + _root.config.lading_page) + '"></iframe>' +
           '<iframe scrolling="no" style="display:none;border:0px;" id="eg-mobile-landscape" width="100%" height="100%" src="' + _root.config.url_banner_rotate + '?origin=' + ogrin + '&clickTAG=' + encodeURIComponent(_root.config.click_redirect + _root.config.lading_page) + '"></iframe>';
       _root.data.ad_click = ad_click;
       ad_content.appendChild(ad_click);
       divMid.appendChild(ad_content);
       _root.data.main_div = mainDiv;
     
       _root.data.check_create = true;
       if (_root.data.site_config.is_detect) {
           _root.data.site_content.addEventListener("scroll", _root.wdScroll, false);
           _root.data.site_content.addEventListener("touchmove", _root.wdScroll, false);
       }
       _window.addEventListener('orientationchange', _root.changeImg);
       _window.addEventListener('resize', _root.changeImg);
       if (_root.data.site_config.is_detect == true) {
           _root.data.obj_div_content.insertBefore(mainDiv, flag);
       } else {
           var window_name = window.name;
           var parrent_div = window.parent.document.getElementsByName(window_name)[0].parentNode.parentNode.parentNode;
           var child_div = window.parent.document.getElementsByName(window_name)[0].parentNode.parentNode;
           parrent_div.insertBefore(mainDiv, child_div);
       }
       _root.data.banner_portrait = _document.getElementById('eg-mobile-portrait');
       _root.data.banner_landscape = _document.getElementById('eg-mobile-landscape');
       _root.changeImg();
   };
   EGMobileInPage.prototype.clickBanner = function () {
       _window.open(_root.config.click_redirect + _root.config.lading_page, "_blank");
       if (_root.config.click_tracking && _root.config.click_tracking != "") {
           new Image().src = _root.config.click_tracking;
       }
   };
   EGMobileInPage.prototype.wdScroll = function () {
       var curent_position = _root.scrollY();
       var windown_height = _root.wdHeight();
       if (_root.data.ad_content.clientHeight > 0) {
           _root.data.main_div.style.opacity = 1;
       } else {
           _root.data.main_div.style.opacity = 0;
       }
       _root.data.main_div.style.height = windown_height + 'px';
       var div_offset_top = _root.posY(_root.data.main_div);
       var div_offset_bottom = div_offset_top + windown_height;        
       if ((curent_position + windown_height) >= div_offset_top && curent_position <= div_offset_bottom + 50) {
           var contentW = _root.data.obj_div_content.clientWidth;
           if (typeof contentW === undefined || contentW === 0) {
               contentW = '100%';
           } else {
               contentL = (_root.wdWidth() - contentW) / 2 + 'px';
               contentW = contentW + 'px';
           }
           _root.data.div_mid.style.opacity = 1;
           _root.data.main_div.style.opacity = 1;
           _root.data.ad_content.style.width = contentW;
           _root.data.ad_content.style.top = 0;
           _root.data.ad_content.style.left = contentL;
           _root.data.ad_content.style.display = 'block';
           _root.data.ad_content.style.position = 'fixed';
           _root.data.ad_content.style.margin = '0 auto';
       } else {
           _root.data.div_mid.style.opacity = 0;
       }
   };
   EGMobileInPage.prototype.changeImg = function () {
       if (_root.data.check_create === true) {
           var w_w = _root.wdWidth();
           var w_h = _root.wdHeight();
           if (w_w >= w_h) {
               _root.data.rotate = "landscape";
               _root.data.banner_portrait.style.setProperty('display','none','important');
               _root.data.banner_landscape.style.setProperty('display','block','important');
           } else {
               _root.data.rotate = "portrait";
               _root.data.banner_portrait.style.setProperty('display','block','important');
               _root.data.banner_landscape.style.setProperty('display','none','important');
           }
           _root.data.div_mid.style.clip = "rect(0px " + w_w + "px " + w_h + "px 0px)";
           var windown_height = _root.wdHeight();
           if (_root.data.client_width != _root.wdWidth()) {
               _root.data.main_div.style.height = windown_height + 'px';
               _root.data.client_width = _root.wdWidth();
           }
       }
   };
   EGMobileInPage.prototype.contentSplit = function (element, spearator) {
       var tagName = element.parentNode;
       var parent = element.parentNode;
       if (element.id.toUpperCase().valueOf() == _root.data.site_config.id_div_content.toUpperCase()) {
           return spearator;
       } else {
           return _root.contentSplit(parent, element);
       }
   };
   EGMobileInPage.prototype.posY = function (elm) {
       var test = elm,
           top = 0;
       while (!!test && test.tagName.toLowerCase() !== "body") {
           top += test.offsetTop;
           test = test.offsetParent;
       }
       return top;
   };
   EGMobileInPage.prototype.scrollY = function () {
       if (_window.pageYOffset) {
           return _window.pageYOffset;
       }
       return Math.max(_document.documentElement.scrollTop, _document.body.scrollTop);
   };
   EGMobileInPage.prototype.wdHeight = function () {
       var a;
       "number" == typeof _window.innerHeight ? a = _window.innerHeight : _root.data.site_content.documentElement && _root.data.site_content.documentElement.clientHeight ? a = _root.data.site_content.documentElement.clientHeight : _root.data.site_content.body && _root.data.site_content.body.clientHeight && (a = _root.data.site_content.body.clientHeight);
       return a;
   };
   EGMobileInPage.prototype.wdWidth = function () {
       var a;
       "number" == typeof _window.innerWidth ? a = _window.innerWidth : _root.data.site_content.documentElement && _root.data.site_content.documentElement.clientWidth ? a = _root.data.site_content.documentElement.clientWidth : _root.data.site_content.body && _root.data.site_content.body.clientWidth && (a = _root.data.site_content.body.clientWidth);
       return a;
   };
   EGMobileInPage.prototype.log = function (msg) {
   };
};
try {
   var EGMobileInPage = new EGMobileInPage(eg_EGMobileInPage_config);
   EGMobileInPage.init();
} catch (error) {
   console.log(error); 
}