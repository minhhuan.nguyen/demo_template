
try {
    var _window = window.top;
    var _document = window.top.document;
} catch (err) {
    _window = window.parent;
    _document = window.parent.document;
}
var uk_InView_Standard = function (config) {
    _root = this;
    console.log(_root);
    this.config = config;
    //function
    this.data = {
        site_content: null,
        site_config: {
            id_content: null,
            rule: null,
            i_show: 0,
            is_detect: true,
        },
        obj_div_content: null,
        client_width: null,
        main_div: null,
        div_mid: null,
        check_create: null,
        iframe: null,
        ukPlayer: null,
        rotate: "portrait",
        portrait: {
            is_play: false,
        },
        landscape: {
            is_play: false,
        },
        event: {
            portrait: {
                source: null,
                origin: null
            },
            landscape: {
                source: null,
                origin: null
            }
        },
        load_banner: false,
    };
    uk_InView_Standard.prototype.insertScript = function () {
        if (_root.data.site_config.srcScript === "") return false;
        var e = document.createElement("script");
        e.type = "text/javascript",
            e.src = _root.data.site_config.srcScript,
            _document.getElementsByTagName("head").item(0).appendChild(e);
    };
    uk_InView_Standard.prototype.insertCSS = function () {
        if (_root.data.site_config.srcCSS === "") return false;
        var e = document.createElement("link");
        e.type = "text/css", e.rel = "stylesheet",
            e.href = _root.data.site_config.srcCSS,
            _document.getElementsByTagName("head").item(0).appendChild(e);
    };
    uk_InView_Standard.prototype.init = function () {
        _root.data.site_content = _document;
        var hostname = window.location.host;
        if (hostname.indexOf('www.') === 0) {
            hostname = hostname.replace('www.', '');
        }
        console.log('Ureka Template InViewStandard host = ' + hostname);
        //get config
        if (hostname == undefined) {
            console.log("UK Can't not get host name");
            return;
        }
        if (uk_InView_Standard_Site_config[hostname] == undefined) {
            console.log("UK Can't not get Site config");
            return;
        }

        if (typeof uk_InView_Standard_Site_config[hostname].id_content != "undefined") {
            _root.data.site_config.id_content = uk_InView_Standard_Site_config[hostname].id_content;
        } else {
            console.log("Can't not load id_content");
        }
        if (typeof uk_InView_Standard_Site_config[hostname].rule != "undefined") {
            _root.data.site_config.rule = uk_InView_Standard_Site_config[hostname].rule;
        } else {
            console.log("Can't not load rule");
        }
        //js and css
        if (typeof uk_InView_Standard_Site_config[hostname].js != "undefined") {
            _root.data.site_config.srcScript = uk_InView_Standard_Site_config[hostname].js;
        }
        if (typeof uk_InView_Standard_Site_config[hostname].css != "undefined") {
            _root.data.site_config.srcCSS = uk_InView_Standard_Site_config[hostname].css;
        }
        //is_detect
        if (typeof uk_InView_Standard_Site_config[hostname].is_detect != "undefined") {
            _root.data.site_config.is_detect = uk_InView_Standard_Site_config[hostname].is_detect;
        }
        _root.insertCSS();
        _root.insertScript();

        if (_root.data.site_config.is_detect == true) {
            _root.analyticContent();
        } else {
            _root.createBanner(null);
        }


    };
    uk_InView_Standard.prototype.analyticContent = function () {
        try {
           _root.data.site_content.getElementById(_root.data.site_config.id_content) ? _root.data.site_content.getElementById(_root.data.site_config.id_content).id : _root.data.site_content.getElementsByClassName(_root.data.site_config.id_content)[0].setAttribute('id', _root.data.site_config.id_content);
           _root.data.obj_div_content = _document.getElementById(_root.data.site_config.id_content);
           //check video content
           var arr_rule = ['p', 'div', 'span'];
           var rule_length = 0;
           for (var i = 0; i < arr_rule.length; i++) {
               var element = arr_rule[i];
               var rule = _root.data.obj_div_content.querySelectorAll(element);
               // console.log(rule);
               if (rule.length > rule_length) {
                   _root.data.site_config.rule = element;
                   _root.data.site_config.show_before = Math.floor(rule.length / 3);
                   rule_length = rule.length;
               }
           }
           _root.getPosition();
       } catch (error) {
           console.log(error); 
       }
    };
    uk_InView_Standard.prototype.getPosition = function () {
        //run banner
        if (!_root.data.obj_div_content) return false;
        var parent = null;
        var flag = _root.data.obj_div_content.getElementsByTagName(_root.data.site_config.rule).item(_root.data.site_config.show_before);
        if (flag !== null) {
            parent = flag.parentNode;
            flag = _root.contentSplit(parent, flag);
            _root.data.client_width = _root.uk_wdWidth();
            _root.createBanner(flag);
        } else {
            if (_root.data.site_config.show_before > 1) {
                _root.data.site_config.show_before--;
                _root.getPosition();
            } else {
                console.log('UK content shorly can not add banner!');
            }
        }
    };
    uk_InView_Standard.prototype.createBanner = function (flag) {
        var mainDiv = document.createElement('div');
        mainDiv.setAttribute('id', 'uk-inview-content')
        mainDiv.style.width = '100%';
        mainDiv.style.height = '100%';
        mainDiv.style.maxWidth = '100%';
        mainDiv.style.overflow = 'initial';
        mainDiv.style.textAlign = 'center';
        mainDiv.style.position = 'relative';
        mainDiv.style.background = 'transparent';
        mainDiv.style.visibility = 'visible';
        mainDiv.style.display = 'block';
        mainDiv.style.opacity = '1';
        mainDiv.style.zIndex = '2';
        var divMid = document.createElement('div');
        divMid.style.width = '100%';
        divMid.style.height = '100%';
        divMid.style.overflow = 'hidden';
        divMid.style.textAlign = 'center';
        divMid.style.position = 'absolute';
        divMid.style.background = 'transparent';
        _root.data.div_mid = divMid;
        mainDiv.appendChild(divMid);
        var ad_content = document.createElement('div');
        ad_content.setAttribute('id', 'uk-mobile-inpage');
        ad_content.style.opacity = 1;
        if (_root.data.obj_div_content != null) {
           ad_content.style.width = _root.data.obj_div_content.clientWidth + "px";
       }
        ad_content.style.transform = 'perspective(0)';
        ad_content.style.height = "100%";
        _root.data.ad_content = ad_content;
        var ad_click = document.createElement('div');
        ad_click.style.cursor = "pointer";
        ad_click.setAttribute('id', 'mobile-inpage-click');
        ad_click.style.width = "100%";
        ad_click.style.height = "100%";
        // ad_click.style.display = "flex";
        if (_root.config.urlBanner != "") {
            
           //  var arrRefer = _document.location;
        
           //  var ogrin = arrRefer.protocol + '//' + arrRefer.hostname;
           //  ad_click.innerHTML = '<iframe scrolling="no" style="border:0px;width:100% !important;height:100% !important d" id="uk-mobile-portrait" width="100%" height="100%" src="' + _root.config.banner + '?origin=' + ogrin + '&clickTAG=' + encodeURIComponent(_root.config.CLICK_REDIRECT_URL_ESC + _root.config.landing_page) + '"></iframe>' +
           // '<iframe scrolling="no" style="display:none;border:0px;" id="uk-mobile-landscape" width="100%" height="100%" src="' + _root.config.banner + '?origin=' + ogrin + '&clickTAG=' + encodeURIComponent(_root.config.CLICK_REDIRECT_URL_ESC + _root.config.landing_page) + '"></iframe>';

            _root.data.ad_click = ad_click;
            ad_content.appendChild(ad_click);
            divMid.appendChild(ad_content);
            _root.data.main_div = mainDiv;

            ad_click.addEventListener('click', function () {
                _root.clickBanner();
            });

            _root.data.check_create = true;
            //_root.intC();
            if (_root.data.site_config.is_detect) {
                _root.data.site_content.addEventListener("scroll", _root.wdScroll, false);
                _root.data.site_content.addEventListener("touchmove", _root.wdScroll, false);
            }
            // _window.addEventListener('orientationchange', _root.changeImg);
            _window.addEventListener('resize', _root.changeImg);
            if (_root.data.site_config.is_detect == true) {
                _root.data.obj_div_content.insertBefore(mainDiv, flag);
            } else {
                var window_name = window.name;
                var parrent_div = window.parent.document.getElementsByName(window_name)[0].parentNode.parentNode.parentNode;
                var child_div = window.parent.document.getElementsByName(window_name)[0].parentNode.parentNode;
                parrent_div.insertBefore(mainDiv, child_div);
            }
            _root.data.banner_portrait = _document.getElementById('uk-mobile-portrait');
            _root.data.banner_landscape = _document.getElementById('uk-mobile-landscape');
            _root.changeImg();
            //call function publicser
            (typeof updateInreadAction !== 'undefined') ? updateInreadAction() : "";
        }
        else {
            console.log("urlBanner");
            margin_bottom =  10;
            mainDiv.style.marginTop = margin_bottom+'px';
            mainDiv.style.marginBottom = margin_bottom+'px';
            var iframe = document.createElement('iframe');
            iframe.setAttribute('id', 'iframe-inview-content');
            iframe.setAttribute('name', 'iframe-inview-content');
            iframe.style.border = "none";
            iframe.style.borderStyle = "none";
            iframe.style.width = '300px';
            iframe.style.height = '600px';
            iframe.scrolling = 'no';
            _root.data.iframe = iframe;
            ad_click.appendChild(iframe);
            ad_content.appendChild(ad_click);
            divMid.appendChild(ad_content);
            _root.data.main_div = mainDiv;
            _root.data.check_create = true;
            // _root.data.site_content.addEventListener("scroll", _root.wdScroll, false);
            // _root.data.site_content.addEventListener("touchmove", _root.wdScroll, false);
            // _root.data.obj_div_content.insertBefore(mainDiv, flag);
            if (_root.data.site_config.is_detect == true) {
                _root.data.site_content.addEventListener("scroll", _root.wdScroll, false);
                _root.data.site_content.addEventListener("touchmove", _root.wdScroll, false);
                _root.data.obj_div_content.insertBefore(mainDiv, flag);
            } else {
                var window_name = window.name;
                var parrent_div = window.parent.document.getElementsByName(window_name)[0].parentNode.parentNode.parentNode;
                var child_div = window.parent.document.getElementsByName(window_name)[0].parentNode.parentNode;
                parrent_div.insertAfter(mainDiv, child_div);
            }
            

            //call function publicser
            (typeof updateInreadAction !== 'undefined') ? updateInreadAction() : "";
        }
    };
    uk_InView_Standard.prototype.clickBanner = function () {
        _window.open(_root.config.CLICK_REDIRECT_URL_ESC + _root.config.landing_page, "_blank");
    };
    uk_InView_Standard.prototype.loadBanner = function (obj) {
        if(!_root.data.load_banner){
            if (_root.config.urlBanner != "") {
                obj.innerHTML = '<img id="uk-mobile-portrait" src="' + _root.config.urlBanner + '" style="width:100% !important;object-fit: contain;"/>' +
                '<img id="uk-mobile-landscape"  src="' + _root.config.urlBanner_rotate + '" style="width:100% !important;object-fit: contain;"/>';
            }else{
                // _root.changeImg();
                var innerDoc = (obj.contentDocument) ? obj.contentDocument : obj.contentWindow.document;
                // console.log(innerDoc);
                innerDoc.open();
                innerDoc.write('<body style="text-align:center;margin:0;padding:0;">' + _root.decodeHtmlEntity(_root.config.banner) + '</body>');
                innerDoc.close();
                _root.changeImg();
                
            }
            _root.data.load_banner = true;
            console.log("banner has loaded")
            
        }
    };
    uk_InView_Standard.prototype.wdScroll = function () {
        var curent_position = _root.scrollY();
        var windown_height = _root.uk_wdHeight();



        if (_root.data.ad_content.clientHeight > 0) {
            _root.data.main_div.style.opacity = 1;
        } else {
            _root.data.main_div.style.opacity = 0;
        }
        _root.data.main_div.style.height = windown_height + 'px';
        var div_offset_top = _root.posY(_root.data.main_div);
        var div_offset_bottom = div_offset_top + windown_height;

        // console.log(curent_position, div_offset_top);
        if(curent_position > div_offset_top - _window.innerHeight){
            if (_root.config.urlBanner != "") {
                _root.loadBanner(_root.data.ad_click);  
            }else{
                _root.loadBanner(_root.data.iframe);
            }
        }


        if ((curent_position + windown_height) >= div_offset_top && curent_position <= div_offset_bottom + 50) {
            var contentW = _root.data.obj_div_content.clientWidth;
            if (typeof contentW === undefined || contentW === 0) {
                contentW = '100%';
            } else {
                contentL = (_root.uk_wdWidth() - contentW) / 2 + 'px';
                contentW = contentW + 'px';
            }
           _root.data.div_mid.style.opacity = 1;
           _root.data.main_div.style.opacity = 1;
           _root.data.ad_content.style.width = contentW;
           _root.data.ad_content.style.top = 0;
           _root.data.ad_content.style.left = contentL;
           _root.data.ad_content.style.display = 'block';
           _root.data.ad_content.style.position = 'fixed';
           _root.data.ad_content.style.margin = '0 auto';
        } else {
            _root.data.div_mid.style.opacity = 0;
        }
    };
    uk_InView_Standard.prototype.changeImg = function () {
        if (_root.data.check_create === true) {
            var w_w = _root.uk_wdWidth();
            var w_h = _root.uk_wdHeight();
            if (_root.config.urlBanner != "") {
                if (w_w >= w_h) {
                    _root.data.rotate = "landscape";
                    _root.data.banner_portrait.style.setProperty('display','none','important');;
                    _root.data.banner_landscape.style.setProperty('display','block','important');
                    // _root.data.banner_landscape.style.height = w_h + "px";

                } else {
                    _root.data.rotate = "portrait";
                    _root.data.banner_portrait.style.setProperty('display','block','important');
                    _root.data.banner_landscape.style.setProperty('display','none','important');
                    // _root.data.banner_portrait.style.height = _root.config.height + "px";
                    // _root.data.banner_portrait.style.width = _root.config.width + "px";
                    // _root.data.banner_portrait.style.margin = 'auto';
                }
            }

            _root.data.div_mid.style.clip = "rect(0px " + w_w + "px " + w_h + "px 0px)";
            var windown_height = _root.uk_wdHeight();
            if (_root.data.client_width != _root.uk_wdWidth()) {
                _root.data.main_div.style.height = windown_height + 'px';
                _root.data.client_width = _root.uk_wdWidth();
            }
        }
    };
    uk_InView_Standard.prototype.contentSplit = function (element, spearator) {
        var tagName = element.parentNode;
        var parent = element.parentNode;
        if (element.id.toUpperCase().valueOf() == _root.data.site_config.id_content.toUpperCase()) {
            return spearator;
        } else {
            return _root.contentSplit(parent, element);
        }
    };
    uk_InView_Standard.prototype.posY = function (elm) {
        var test = elm,
            top = 0;
        while (!!test && test.tagName.toLowerCase() !== "body") {
            top += test.offsetTop;
            test = test.offsetParent;
        }
        return top;
    };
    uk_InView_Standard.prototype.decodeHtmlEntity = function (str) {
        return str.replace(/&#(\d+);/g, function (match, dec) {
            return String.fromCharCode(dec);
        });
    };
    uk_InView_Standard.prototype.encodeHtmlEntity = function (str) {
        var buf = [];
        for (var i = str.length - 1; i >= 0; i--) {
            buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
        }
        return buf.join('');
    };
    uk_InView_Standard.prototype.scrollY = function () {
        if (_window.pageYOffset) {
            return _window.pageYOffset;
        }
        return Math.max(_document.documentElement.scrollTop, _document.body.scrollTop);
    };
    uk_InView_Standard.prototype.uk_wdHeight = function () {
        var a;
        "number" == typeof window.top.innerHeight ? a = window.top.innerHeight : window.top.document.documentElement && window.top.document.documentElement.clientHeight ? a = window.top.document.documentElement.clientHeight : window.top.document.body && window.top.document.body.clientHeight && (a = window.top.document.body.clientHeight);
        return a;
    };
    uk_InView_Standard.prototype.uk_wdWidth = function () {
        var a;
        "number" == typeof _window.innerWidth ? a = _window.innerWidth : _root.data.site_content.documentElement && _root.data.site_content.documentElement.clientWidth ? a = _root.data.site_content.documentElement.clientWidth : _root.data.site_content.body && _root.data.site_content.body.clientWidth && (a = _root.data.site_content.body.clientWidth);
        return a;
    };
    uk_InView_Standard.prototype.log = function (msg) {
        console.log('Ureka TP InView Log: ' + msg);
    };
};

window.top.ukInviewDestroy = function() {
	var container = window.top.document.getElementById("uk-inview-content");
	if (container) {
		container.style.setProperty("display", "none", "important");
		container.style.setProperty("height", "0", "important");
		console.log("No deals");
	}
};

try {
    var InviewMB = new uk_InView_Standard(uk_InView_Standard_config);
    InviewMB.init()
} catch (error) {

}