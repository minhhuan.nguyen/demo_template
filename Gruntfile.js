module.exports = function(grunt) {
    grunt.initConfig({

        uglify: {
            js_mobile: {
                src: 'src/uk_InViewStandard.js',
                dest: 'build/uk_InViewStandard.min.js',
            },
            options: {
                output: {
                    comments: /^!/
                }
            }
        },
        jsObfuscate: {
            test: {
                options: {
                    concurrency: 2,
                    keepLinefeeds: false,
                    keepIndentations: false,
                    encodeStrings: false,
                    encodeNumbers: false,
                    moveStrings: true,
                    replaceNames: false,
                    variableExclusions: ['^_get_', '^_set_', '^_mtd_']
                },
                files: {
                    'build/uk_InViewStandard.min.obf.js': [
                        'build/uk_InViewStandard.min.js',
                    ]
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('js-obfuscator');

    grunt.registerTask('default', ['uglify', 'jsObfuscate']);
};